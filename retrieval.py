import cv2
import numpy as np


def filter_boxes(boxes, len_points, th = 0.1):
    final_boxes = []
    
    boxes = np.array(boxes)
    x0 = boxes[:, 1]
    y0 = boxes[:, 0]
    x1 = boxes[:, 3]
    y1 = boxes[:, 2]
    ars = (x1 - x0) * (y1 - y0)

    ids = np.argsort(len_points)
    
    while len(ids) > 0:

        final_boxes.append(boxes[ids[-1]])

        x_1 = np.maximum(x0[ids[-1]], x0[ids[:-1]])
        x_2 = np.minimum(x1[ids[-1]], x1[ids[:-1]])
        y_1 = np.maximum(y0[ids[-1]], y0[ids[:-1]])
        y_2 = np.minimum(y1[ids[-1]], y1[ids[:-1]])

        inter = np.maximum(0.0, x_2 - x_1 + 1) * np.maximum(0.0, y_2 - y_1 + 1)
        iou = inter / (ars[ids[-1]] + ars[ids[:-1]] - inter)
        ids = ids[np.where(iou < th)]

    return final_boxes


def predict_image(img: np.ndarray, query: np.ndarray) -> list:
    
    gallery = img.copy()
    query_shape = (137, 65)
    padd = 10
    k = 6
    
    gallery = cv2.cvtColor(gallery, cv2.COLOR_BGR2GRAY)
    query = cv2.cvtColor(query, cv2.COLOR_BGR2GRAY)

    scale = (query_shape[1] + 25) / query.shape[1]

    gallery = cv2.resize(gallery, (0,0), fx=scale, fy=scale) 
    query = cv2.resize(query, (0,0), fx=scale, fy=scale)

    query = query[padd:-padd, padd:-padd]
    
    len_points = []
    boxes = []
    
    query_shape = query.shape
    gallery_shape = gallery.shape
    
    for i in range(0, gallery_shape[0] - query_shape[0], query_shape[0] // k):
        for j in range(0 , gallery_shape[1] - query_shape[1], query_shape[1] // k):
            im = gallery[i : query_shape[0] + i, j : query_shape[1] + j]   
            try:

                sift = cv2.SIFT_create()

                kp1, des1 = sift.detectAndCompute(query,None)
                kp2, des2 = sift.detectAndCompute(im,None)

                FLANN_INDEX_KDTREE = 1

                index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
                flann = cv2.FlannBasedMatcher(index_params)
                matches = flann.knnMatch(des1.astype(np.float32), des2.astype(np.float32), k=2)

                good = []
                for m,n in matches:
                    if m.distance < 0.7 * n.distance:
                        good.append(m)

                src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1,1,2)
                dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1,1,2)

                len_points.append(len(dst_pts))
                boxes.append([i, j, i + query_shape[0], j + query_shape[1]])

            except Exception:
                continue
                
    len_points = np.array(len_points)
    boxes = np.array(boxes)

#     boxes[:,0 : 2] -= padd
#     boxes[:,2 : 4] += padd
    res = []
    if len(boxes) > 0:
        final_boxes = filter_boxes(boxes[len_points > 20], len_points[len_points > 20], 0.05)
        for box in final_boxes:
            x1, y1, x2, y2 = box[0], box[1], box[2], box[3]
            
            x1 -= padd
            y1 -= padd
            x2 += padd
            y2 += padd
            
            w = y2 - y1
            h = x2 - x1

            
            res.append([x1 / gallery_shape[0], y1 / gallery_shape[1], w / gallery_shape[1], h / gallery_shape[0]])
    else:
        res = [(0, 0, 1, 1)]
              
    return res
